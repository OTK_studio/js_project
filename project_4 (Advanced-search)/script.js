const container = document.querySelector(".s-container");
inputBtn = container.querySelector(".search-input");
forms = container.querySelector(".form-search");
i_Btn = container.querySelector(".searchs");
close_Btn = container.querySelector(".s-close");
options = container.querySelector(".s-options");
searchInp = container.querySelector("input");

let objects = ['Phone','computer','card ID','Car','Charger','Books']

// document.body.addEventListener("click", fn, true)
// function fn(){
//     container.classList.remove("active")
//     forms.classList.remove("active")
//     setTimeout(function(){ i_Btn.style.display="block" },10000) 
// }

close_Btn.addEventListener("click", () => {
    container.classList.remove("active");
    forms.classList.remove("active");
    i_Btn.style.display="block";
    searchInp.value="";
});


i_Btn.addEventListener("click", () => {
    forms.classList.toggle("active")
    i_Btn.style.display="none"
    close_Btn.style.display="block"
});



function AllObjects(){
    options.innerHTML="";
    objects.forEach(obj => {
        let li = `<li onclick="getName(this)">${obj}</li>`
        options.insertAdjacentHTML("beforeend", li)
    })
}

AllObjects()

function getName(selected){
    console.log(selected.innerText)
    searchInp.value="";
    AllObjects();
    container.classList.remove("active")
    inputBtn.value=selected.innerText;
}

searchInp.addEventListener("keyup", () => {
    let arr =[];
    let searchval = searchInp.value;
    arr = objects.filter(data =>{
        return data.toLocaleLowerCase().startsWith(searchval)
    }).map(data => `<li onclick="getName(this)" >${data}</li>`).join("");
    options.innerHTML = arr ? arr: `<p>Oops ! Nothing Found</p>` ;
    console.log(arr)
});

inputBtn.addEventListener("click", () => {
    container.classList.toggle("active")
});
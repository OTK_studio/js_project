const wrapper = document.querySelector(".wrapper");
selectBtn = wrapper.querySelector(".select_btn");
options = wrapper.querySelector(".options");
searchInp = wrapper.querySelector("input");
let objects = ['Phone','computer','card ID','Car','Charger','Books']
function AllObjects(){
    options.innerHTML="";
    objects.forEach(obj => {
        let li = `<li onclick="getName(this)">${obj}</li>`
        options.insertAdjacentHTML("beforeend", li)
    })
}

AllObjects()

function getName(selected){
    // console.log(selected.innerText)
    searchInp.value="";
    AllObjects();
    wrapper.classList.remove("active")
    // selectBtn.firstElementChild.innerText=selected.innerText;
}

searchInp.addEventListener("keyup", () => {
    let arr =[];
    let searchval = searchInp.value;
    arr = objects.filter(data =>{
        return data.toLocaleLowerCase().startsWith(searchval)
    }).map(data => `<li onclick="getName(this)" >${data}</li>`).join("");
    options.innerHTML = arr ? arr: `<p>Oops ! Nothing Found</p>` ;
    console.log(arr)
});

selectBtn.addEventListener("click", () => {
    wrapper.classList.toggle("active")
});
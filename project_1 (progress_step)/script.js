const progress = document.getElementById('progress');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
const circles = document.querySelectorAll('.circle');
let currentAcive = 1;
next.addEventListener('click', () =>{
    currentAcive++;
    if(currentAcive > circles.length){
        currentAcive = circles.length
    }
    update();
})

prev.addEventListener('click', () =>{
    currentAcive--;
    if(currentAcive < 1){
        currentAcive = 1;
    }
    update();
})

function update(){
    circles.forEach((circle, idx) =>{
        if(idx < currentAcive){
            circle.classList.add('active');
        }else{
            circle.classList.remove('active');
        }
    })
    const actives = document.querySelectorAll('.active');
    progress.style.width = (actives.length-1) / (circles.length-1) * 100 + '%';
    if(currentAcive === 1){
        prev.disabled = true
    } 
    if(currentAcive === circles.lenght){
        next.disabled = true
    } 
    else {
        prev.disabled = false
        next.disabled = false  
    }
}
